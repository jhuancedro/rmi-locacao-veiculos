#!/usr/bin/env bash

echo "[INICIANDO TESTES]"

echo "[REMOVENDO DADOS ANTIGOS]"
rm *.csv

echo "[COMPILANDO CODIGOS]"
javac */*.java

echo "[LIGANDO SERVIDORES]"
gnome-terminal -x java cadastro.ServidorCadastro servidor-cadastro 5000 false
sleep 1
gnome-terminal -x java filial.ServidorFilial filial1 5001 servidor-cadastro 5000 false
gnome-terminal -x java filial.ServidorFilial filial2 5002 servidor-cadastro 5000 false
sleep 1

echo "[PROCEDIMENTO DE TESTE]"
java src.Testes

echo "[LIMPANDO ARQUIVOS DE BUILD]"
rm */*.class

echo "[TESTES FINALIZADOS]"