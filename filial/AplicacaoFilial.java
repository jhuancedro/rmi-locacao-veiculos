package filial;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/** Aplicacao da filial que fara as solicitacoes ao servidor da filial */
public class AplicacaoFilial {
    private static String nomeSFilial = "servidor-filial";
    private int portaSFilial;
    private Filial servidorFilial;

    public AplicacaoFilial(int portaSFilial) {
        this.portaSFilial = portaSFilial;

        System.out.print(String.format("[%04d] ", portaSFilial));
        try {
            localizaServidorFilial();
            System.out.println(String.format("Aplicação localizou servidor da filial de porta '%04d' com sucesso.", portaSFilial));
        } catch (Exception ex) {
            System.err.println(String.format("Nao possivel à aplicação localizar o servidor da filial de porta '%04d'.", portaSFilial));
            ex.printStackTrace();
        }
    }

    /** Localiza o servidor da filial */
    private void localizaServidorFilial() throws RemoteException, NotBoundException, MalformedURLException {
        servidorFilial = (Filial) Naming.lookup(String.format("rmi://localhost:%04d/%s", portaSFilial, nomeSFilial));
    }

    /** Solicita ao servidor da filial que cadastre o cliente */
    public int cadastrar(String nomeCliente) throws RemoteException {
        int numCliente = servidorFilial.cadastrar(nomeCliente);

        System.out.print(String.format("[%04d] ", portaSFilial));
        if (numCliente == 0)
            System.out.println(String.format("Não foi possível cadastrar o cliente de nome '%s'.", nomeCliente));
        else
            System.out.println(String.format("Cliente de nome '%s' cadastrado sob o número '%04d'.", nomeCliente, numCliente));

        return numCliente;
    }

    /** Solicita ao servidor da filial que realize a efetivarLocacao de um veiculo */
    public boolean locacao(int veiculo, int numCliente) throws IOException {
        boolean ok = servidorFilial.solicitarLocacao(veiculo, numCliente);

        System.out.print(String.format("[%04d] ", portaSFilial));
        if (ok)
            System.out.println(String.format("Veiculo '%04d' alocado ao cliente de número '%s'.", veiculo, numCliente));
        else
            System.out.println(String.format("Não foi possível alocar veiculo '%04d' ao cliente de numero '%s'.", veiculo, numCliente));

        return ok;
    }

    /** Solicita ao servidor da filial que realize a devolucao de um veiculo */
    public boolean devolucao(int veiculo) throws IOException {
        boolean ok = servidorFilial.devolucao(veiculo);

        System.out.print(String.format("[%04d] ", portaSFilial));
        if (ok)
            System.out.println(String.format("Veiculo '%04d' devolvido.", veiculo));
        else
            System.out.println(String.format("Não foi possível devolver veiculo '%04d'.", veiculo));

        return ok;

    }

    /** Solicita ao servidor da filial que descadastre o cliente */
    public boolean descadastrar(int numCliente) throws IOException {
        System.out.print(String.format("[%04d] ", portaSFilial));

        boolean ok = servidorFilial.descadastrar(numCliente);
        if(ok)
            System.out.println(String.format("Cliente de numero '%04d' descadastrado.", numCliente));
        else
            System.out.println(String.format("Não foi possível descadastrar cliente de numero '%04d'.", numCliente));

        return ok;
    }

    /** Solicita ao servidor da filial que remove seu historico de locacoes */
    public void limpaLocacoes() {
        System.out.print(String.format("[%04d] ", portaSFilial));

        try {
            servidorFilial.limparLocacoes();
            System.out.println(String.format("Hitorico de locações da filial de porta %04d deletado.", portaSFilial));
        } catch (IOException e) {
            System.out.println(String.format("Não foi possível deletar o histórico de locações da filial de porta %04d deletado.", portaSFilial));
            e.printStackTrace();
        }
    }

    /** Solicita o desligamento do servidor da filial */
    public void shutdown() {
        System.out.print(String.format("[%04d] ", portaSFilial));

        try {
            servidorFilial.shutdown();
        } catch (RemoteException e) {
//            e.printStackTrace();
        }
        System.out.println(String.format("Servidor da filial de porta '%04d' desligado.", portaSFilial));
    }

    /** Solicita o desligamento do servidor de cadastro */
    public void shutdownCadastro() {
        System.out.print(String.format("[%04d] ", portaSFilial));

        try {
            servidorFilial.shutdownCadastro();
        } catch (RemoteException e) {
//            e.printStackTrace();
        }
        System.out.println("Servidor de cadastro desligado.");
    }

}
