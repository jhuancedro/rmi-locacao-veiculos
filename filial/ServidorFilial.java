package filial;

import cadastro.Cadastro;

import java.io.*;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;

public class ServidorFilial extends UnicastRemoteObject implements Filial {
    private Cadastro servidorCadastro;
    private String arquivoLocacoes;
    private int porta, portaServidorCadastro;
    private String nomeFilial, nomeServidorCadastro;
    private String nomeServidorFilial = "servidor-filial";

    private ServidorFilial(String nomeFilial, int porta, String nomeServidorCadastro, int portaCadastro) throws IOException {
        super();
        this.porta = porta;
        this.nomeFilial = nomeFilial;
        this.portaServidorCadastro = portaCadastro;
        this.nomeServidorCadastro  = nomeServidorCadastro;

        arquivoLocacoes = "locacoes["+nomeFilial+"].csv";
        File file_cadastros = new File(arquivoLocacoes);
        file_cadastros.createNewFile(); // if file already exists will do nothing

        localizaServidorCadastro();
    }

    /** Lista todos as locacoes em aberto */
    @Override
    public void listarLocacoes() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(arquivoLocacoes)));
        String linha;
        System.out.println("\t-------------------");
        System.out.println("\t[cliente, veículo]");
        while ((linha = reader.readLine()) != null) {
            String[] dadosUsuario = linha.split(",");
            System.out.println("\t"+ Arrays.toString(dadosUsuario));
        }
        reader.close();
        System.out.println("\t-------------------");
    }

    /** Poe servidor de pe */
    private void up() {
        try {
            Registry registry = LocateRegistry.createRegistry(porta);
            registry.rebind(nomeServidorFilial, this);
            System.out.println("Servidor '"+ nomeFilial +"' está online na porta '"+String.valueOf(porta)+"'.");
        } catch (Exception ex){
            System.err.println(String.format("Nao foi possivel inicializar o servidor da filial '%s' na porta '%04d'", nomeFilial, porta));
            ex.printStackTrace();
        }
    }

    /** Finaliza processo, desligando o servidor */
    @Override
    public void shutdown() throws RemoteException{
        System.out.println(String.format("Desligando servidor da filial de porta '%04d'", porta));
        System.exit(0);

        // FIXME: Desligar servidor sem deixar de responder o cliente
    }

    /** Localiza o servidor de cadastros */
    private void localizaServidorCadastro() {
        try {
            servidorCadastro = (Cadastro) Naming.lookup(String.format("rmi://localhost:%04d/%s", portaServidorCadastro, nomeServidorCadastro));
            System.err.println(String.format("Filial '%s' de porta %04d localizou o servidor de cadastro na porta %04d",
                    nomeFilial, porta, portaServidorCadastro));
        } catch (Exception ex) {
            System.err.println(String.format("Filial '%s' de porta %04d nao pode localizar servidor de cadastro na porta %04d",
                    nomeFilial, porta, portaServidorCadastro));
            ex.printStackTrace();
        }
    }

    /** Solicita o cadastro de um novo cliente */
    public int cadastrar(String nome) throws RemoteException {
        return servidorCadastro.cadastrarCliente(nome, porta);
    }

    @Override
    public boolean descadastrar(int numCliente) throws IOException {
        return servidorCadastro.descadastrar(numCliente);
    }

    /** Verifica se um cliente possui seu numero no arquivo de locacoes */
    @Override
    public boolean possuiDebito(int numCliente) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(arquivoLocacoes)));
        String linha;
        String strCliente = String.format("%04d", numCliente);
        boolean debito = false;

        while ((linha = reader.readLine()) != null) {
            String[] dadosUsuario = linha.split(",");
            if (dadosUsuario[0].equals(strCliente)){
                debito = true;
                break;
            }
        }
        reader.close();

        return debito;
    }

    /** Inclui efetivarLocacao (veiculo, cliente) ao arquivo de locacoes
     *  retorna true caso tenha sido possivel realizar a efetivarLocacao */
    @Override
    public boolean efetivarLocacao(int veiculo, int numCliente) throws IOException {
        if (this.possuiDebito(numCliente)){
            return false;
        }

        try {
            FileWriter fw = new FileWriter(arquivoLocacoes, true);
            fw.write(String.format("%04d,%04d\n", numCliente, veiculo));
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /** Solicita ao servidor de cadastro a locacao de um veiculo por um cliente */
    @Override
    public boolean solicitarLocacao(int veiculo, int numCliente) throws IOException {
        return servidorCadastro.solicitarLocacao(veiculo, numCliente);
    }

    /** Remove dado de uma efetivarLocacao do arquivo de locacoes */
    @Override
    public boolean devolucao(int veiculo) throws IOException {
        String arquivo_tmp = arquivoLocacoes + ".tmp";
        boolean devolvido = false;

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(arquivoLocacoes)));
        FileWriter fw = new FileWriter(arquivo_tmp);
        String linha;
        String strVeiculo = String.format("%04d", veiculo);

        // rescreve linhas do arquivo de locacoes em um temporario, exceto linha com o veiculo escolhido
        while ((linha = reader.readLine()) != null) {
            String[] dadosUsuario = linha.split(",");
            if (dadosUsuario[1].equals(strVeiculo)){
                devolvido = true;
            } else {
                fw.write(linha+"\n");
            }
        }
        reader.close();
        fw.close();

        // apara o arquivo de locacoes e substitui pelo tempotario
        File file_loc = new File(arquivoLocacoes);
        File file_tmp = new File(arquivo_tmp);
        devolvido = devolvido && file_loc.delete();
        devolvido = devolvido && file_tmp.renameTo(file_loc);
        file_tmp.delete();

        return devolvido;
    }

    /** Apaga o arquivo de locacoes */
    @Override
    public void limparLocacoes() throws IOException {
        System.out.println("Limpando historico de locações.");
        File fileLocacoes = new File(arquivoLocacoes);
        fileLocacoes.delete();
        fileLocacoes.createNewFile();
    }

    @Override
    public void shutdownCadastro() throws RemoteException {
        servidorCadastro.shutdown();
    }

    /** Cria servidor e poe de pe
     * Uso da classe:
     *      java ServidorFilial nome-do-servidor-filial   porta-do-servidor-filial   \
     *                          nome-do-servidor-cadastro porta-do-servidor-cadastro \
     *                          boolean-reset-locacoes */
    public static void main(String args[]) throws IOException {
        String nomeSFilial = args[0];
        int portaSFilial = Integer.valueOf(args[1]);
        String nomeSCadastro = args[2];
        int portaSCadastro = Integer.valueOf(args[3]);
        boolean resetLocacoes = Boolean.valueOf(args[4]);

        System.out.println(String.format("##########\tServidor da Filial '%s'\t##########", nomeSFilial));
        ServidorFilial serverFilial = new ServidorFilial(nomeSFilial, portaSFilial, nomeSCadastro, portaSCadastro);
        serverFilial.up();
        if (resetLocacoes){
            System.out.println("Limpando locações.");
            serverFilial.limparLocacoes();
        }
        else{
            System.out.println("Lista de locações em aberto:");
            serverFilial.listarLocacoes();
        }
    }
}
