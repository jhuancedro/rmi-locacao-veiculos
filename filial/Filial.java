package filial;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/** Interface para o servidor da filial */
public interface Filial extends Remote {
    boolean descadastrar(int numCliente) throws IOException;

    boolean possuiDebito(int numCliente) throws IOException;

    boolean  efetivarLocacao(int veiculo, int numCliente) throws IOException;
    boolean solicitarLocacao(int veiculo, int numCliente) throws IOException;
    boolean devolucao(int veiculo) throws IOException;

    int     cadastrar(String nome) throws RemoteException;
    void limparLocacoes() throws IOException;

    void shutdownCadastro() throws RemoteException;

    void listarLocacoes() throws IOException;

    void shutdown() throws RemoteException;
}
