package src;

import filial.AplicacaoFilial;

import java.io.IOException;

/** Procedimento para testar funcionalidades do sistema */
public class Testes {
    public static void main(String[] args) throws IOException {
        int u1, u2;

        System.out.println("\n#####\tInicializando aplicações:\t#####");
        AplicacaoFilial appf1 = new AplicacaoFilial(5001);
        AplicacaoFilial appf2 = new AplicacaoFilial(5002);
        // appf1.limpaLocacoes();
        // appf2.limpaLocacoes();

        System.out.println("\n#####\tCadastro de clientes:\t#####");
        u1 = appf1.cadastrar("João");   // cadastra usuario 1 na filial 1
        u2 = appf2.cadastrar("José");   // cadastra usuario 2 na filial 2

        System.out.println("\n#####\tCasos de locacao em uma mesma filial:\t#####");
        appf1.locacao(1, u1); // usuario 1 aluga veiculo 1
        appf1.locacao(2, u1); // usuario 1 tenta alugar veiculo 2, enquanto deve o veiculo 1
        appf1.devolucao(1);   // usuario 1 devolve veiculo 1
        appf1.locacao(2, u1); // usuario 1 aluga veiculo 2
        System.in.read();

        System.out.println("\n#####\tCasos de locacao em filiais diferentes:\t#####");
        appf1.locacao(3, u2); // usuario 2 aluga veiculo 3 na filial 1
        appf2.locacao(4, u2); // usuario 2 tenta alugar veiculo 4 na filial 2, enquanto deve o veiculo 3
        appf1.locacao(4, u2); // usuario 2 tenta alugar veiculo 4 na filial 1, enquanto deve o veiculo 3
        System.in.read();

        System.out.println("\n#####\tCasos de locacao em filiais diferentes:\t#####");
        appf2.devolucao(3);   // usuario 2 devolve veiculo 3
        appf2.locacao(4, u2); // usuario 2 aluga veiculo 3
        System.in.read();

        System.out.println("\n#####\tCasos descadastramento:\t#####");
        appf1.descadastrar(u2);      // usuario 2 tenta descadastrar-se na filial 1, enquanto deve o veiculo 4
        appf2.descadastrar(u2);      // usuario 2 tenta descadastrar-se na filial 2, enquanto deve o veiculo 4
        appf2.devolucao(4);   // usuario 2 devolve veiculo 3
        appf2.descadastrar(u2);      // usuario 3 descadastrar-se na filial 2
        System.in.read();

        System.out.println("\n#####\tDesligando servidores:\t#####");
        appf1.shutdownCadastro();   // aproveita "conexao" da filial para desligas o servidor de cadastro

        appf1.shutdown();   // desliga o servidor da filial 1
        appf2.shutdown();   // desliga o servidor da filial 2

    }
}
