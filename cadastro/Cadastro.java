package cadastro;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/** Interface para o servidor de Cadastro */
public interface Cadastro extends Remote {
    int cadastrarCliente(String nomeCliente, int portaFilial) throws RemoteException;
    boolean solicitarLocacao(int veiculo, int numCliente) throws IOException;
    void listarCadastros() throws IOException;

    void shutdown() throws RemoteException;

    boolean descadastrar(int numCliente) throws IOException;
}
