package cadastro;

import filial.Filial;

import java.io.*;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;
import java.util.Random;

public class ServidorCadastro extends UnicastRemoteObject implements Cadastro {
    private static int num_max_cadastro = 10000;
    private String arquivoCadastro = "cadastro.csv";
    private int porta;
    private String nomeServidor;
    private Registry registry;

    private ServidorCadastro(int porta, String nomeServidor) throws IOException {
        super();

        this.porta = porta;
        this.nomeServidor = nomeServidor;

        File file_cadastros = new File(arquivoCadastro);
        file_cadastros.createNewFile();

        registry = LocateRegistry.createRegistry(porta);

        // this.up();
    }

    /** Adiciona cliente ao cadastro e retorna seu numero */
    @Override
    public int cadastrarCliente(String nomeCliente, int portaFilial) throws RemoteException{
        int num = new Random().nextInt(num_max_cadastro-2) + 1; // Numeros de 0001 a 9999
        try {
            FileWriter fw = new FileWriter(arquivoCadastro, true);
            fw.write(String.format("%04d,%d,%s\n", num, portaFilial, nomeCliente));
            fw.close();
        }catch (IOException e) {
            e.printStackTrace();
            return 0;
        }

        return num;
    }

    /** Busca porta da filial na qual o cliente esta cadastrado */
    private int buscaPortaFilial(int numCliente) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(arquivoCadastro)));
        String linha;
        String strCliente = String.format("%04d", numCliente);
        int portaFilial = 0;

        while ((linha = reader.readLine()) != null) {
            String[] dadosUsuario = linha.split(",");
            if (dadosUsuario[0].equals(strCliente)){
                portaFilial = Integer.valueOf(dadosUsuario[1]);
                break;
            }
        }
        reader.close();

        if(portaFilial == 0){
            System.out.println(String.format("Não foi possivel encontrar filial do cliente %04d", numCliente));
        }

        return portaFilial;
    }


    /** Localiza-se ao servidor de cadastros */
    private Filial localizarFilial(int portaFilial) {
        Filial servidorFilial = null;
        try {
            servidorFilial = (Filial) Naming.lookup(String.format("rmi://localhost:%04d/servidor-filial", portaFilial));
            System.out.println(String.format("Filial de porta '%04d' localizada com sucesso.", portaFilial));
        } catch (Exception ex) {
            System.err.println(String.format("Nao foi possivel localizar a filial de porta %04d", portaFilial));
            ex.printStackTrace();
        }

        return servidorFilial;
    }

    @Override
    public boolean solicitarLocacao(int veiculo, int numCliente) throws IOException {
        int portaFilialCliente = buscaPortaFilial(numCliente);
        Filial servidorFilial = localizarFilial(portaFilialCliente);
        if (servidorFilial == null){
            System.out.println(String.format("Nao foi possivel localizar filial de porta %04d.", portaFilialCliente));
            return false;
        } else {
            return servidorFilial.efetivarLocacao(veiculo, numCliente);
        }
    }

    /** Lista todos os clientes cadastrados */
    @Override
    public void listarCadastros() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(arquivoCadastro)));
        String linha;
        System.out.println("\t-------------------");
        System.out.println("\t[num, filial, nome]");
        while ((linha = reader.readLine()) != null) {
            String[] dadosUsuario = linha.split(",");
            System.out.println("\t"+Arrays.toString(dadosUsuario));
        }
        reader.close();
        System.out.println("\t-------------------");
    }

    /** Poe servidor de pe */
    private void up() {
        try {
            registry.rebind(nomeServidor, this);
            System.out.println("Servidor '"+ nomeServidor +"' está online na porta '"+String.valueOf(porta)+"'.");
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /** Finaliza processo, desligando o servidor */
    @Override
    public void shutdown() throws RemoteException{
        System.out.println(String.format("Desligando servidor de cadastro de porta '%04d'", porta));
        System.exit(0);

        // FIXME: Desligar servidor sem deixar de responder o cliente
        /*try {
            registry.unbind(nomeServidor);
            UnicastRemoteObject.unexportObject(registry, true);
            System.out.println(String.format("Servidor de cadastro da porta '%04d' desligado.", porta));
            return true;
        } catch (NotBoundException e) {
            System.out.println(String.format("Não foi possível desligar servidor de cadastro da porta '%04d'.", porta));
            e.printStackTrace();
            return false;
        }*/
    }

    /** Remove dado de uma efetivarLocacao do arquivo de locacoes */
    @Override
    public boolean descadastrar(int numCliente) throws IOException {
        int portaFilial = buscaPortaFilial(numCliente);
        Filial servidorFilial = localizarFilial(portaFilial);
        if (servidorFilial.possuiDebito(numCliente)){
            return false;
        }

        String arquivo_tmp = arquivoCadastro + ".tmp";
        boolean descadastrado = false;

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(arquivoCadastro)));
        FileWriter fw = new FileWriter(arquivo_tmp);
        String linha;
        String strNumCliente = String.format("%04d", numCliente);

        // rescreve linhas do arquivo de locacoes em um temporario, exceto linha com o veiculo escolhido
        while ((linha = reader.readLine()) != null) {
            String[] dadosUsuario = linha.split(",");
            if (dadosUsuario[0].equals(strNumCliente)){
                descadastrado = true;
            } else {
                fw.write(linha+"\n");
            }
        }
        reader.close();
        fw.close();

        // apara o arquivo de locacoes e substitui pelo tempotario
        File file_cad = new File(arquivoCadastro);
        File file_tmp = new File(arquivo_tmp);
        descadastrado = descadastrado && file_cad.delete();
        descadastrado = descadastrado && file_tmp.renameTo(file_cad);
        file_tmp.delete();

        return descadastrado;
    }

    /** Apaga o arquivo de cadastros */
    private void limparCadastros() throws IOException {
        File file = new File(arquivoCadastro);
        file.delete();
        file.createNewFile();
    }

    /** Cria servidor e poe de pe
     * Uso da classe:
     *      java ServidorCadastro nome-do-servidor-cadastro porta-do-servidor-cadastro boolean-reset-cadastros */
    public static void main(String args[]) throws IOException {
        System.out.println("##########\tServidor de Cadastro\t##########");
        String nome_servidor = args[0];
        int porta = Integer.valueOf(args[1]);
        boolean resetCadastros = Boolean.valueOf(args[2]);

        ServidorCadastro servidorCadastro = new ServidorCadastro(porta, nome_servidor);
        servidorCadastro.up();
        if (resetCadastros){
            System.out.println("Limpando cadastros.");
            servidorCadastro.limparCadastros();
        }
        else{
            System.out.println("Lista de Cadastrados:");
            servidorCadastro.listarCadastros();
        }
    }
}